"""
This Module provides several functions to convert a .dbc file into other structures.
Currently supports conversion to XML, JSON and C Message Structs

C Struct:
    
// msg_<Message Name> 0x<ID>
#define CAN_ID_msg_<Message Name> 0x<ID>
typedef struct __attribute__((packed)) {
    // Structure of Message as followed:
    // Signal with length == variable length(-> uint8_t, uint16_t, uint32_t)
    uint32_t sig_<Signale Name>;
    // Signal with length != variable length(-> uint8_t, uint16_t, uint32_t)
    uint16_t sig_<Signale Name> : <Bits>
    // Stuffing Bits:
    uint8_t : <Stuffing Bits>
} msg_<Message Name>

"""

# imports
from dis import code_info
from email import message
import os.path
import json
from datetime import datetime
from pyexpat.errors import messages

" Create a header file with specified name"
def createHeader(headername):
    return "\n//" + "="*67 + "\n// " + headername + "\n//" + "="*67 + "\n"   # Create Header

" Converts a .dbc file to a list of message dicionarys "
def readDBC(filename, srcdir):
    with open(srcdir + "/" + filename, "r") as file:
        dbclines = file.readlines()

    messages = []
    signals = []
    for line in dbclines:
        data = line.split(" ")

        # Extract messages:
        if len(data) > 1 and data[0] == "BO_":
            msg_ID = "0x{:X}".format(int(data[1]))
            msg_long = data[2][:-1]
            DLC = int(data[3])
            msg_sdr = data[4][:-1]
            msg_short = msg_long.removeprefix("msg_" + msg_sdr + "_")
            messages.append({
                "ID": msg_ID,
                "Fullname": msg_long,
                "Shortname": msg_short,
                "DLC": DLC,
                "Sender": msg_sdr
            })

        # Extract signals:
        elif len(data) > 1 and not data[0] and data[1] == "SG_":
            # copy values
            sig_name = data[2]
            m = 1 if data[3].find("m") != -1 or data[3].find("M") != -1 else 0
            fill = 1 if len(data) == 9 else 0
            start = data[4+m]
            end = data[4+m].find("|")
            sig_start = int(start[0:end])
            start = end
            end = data[4+m].find("@")
            sig_length = int(data[4+m][start+1:end])
            start = data[5+m].find("(")
            end = data[5+m].find(",")
            sig_factor = float(data[5+m][start+1:end])
            start = end
            end = data[5+m].find(")")
            sig_offset = int(data[5+m][start+1:end])
            start = data[6+m].find("[")
            end = data[6+m].find("|")
            sig_min = int(round(float(data[6+m][start+1:end]))) # Rounding is not optimal, but it will suffice
            start = end
            end = data[6+m].find("]")
            sig_max = int(round(float(data[6+m][start+1:end]))) # Rounding is not optimal, but it will suffice
            sig_unit = data[7+m][1:-1]
            sig_recv = data[9+m-fill][:-1].split(",")
        # append signal as dictionary
            signals.append({
                "ID": msg_ID,
                "Name": sig_name,
                "Startbit": sig_start,
                "Length": sig_length,
                "Factor": sig_factor,
                "Offset": sig_offset,
                "Minimum": sig_min,
                "Maximum": sig_max,
                "Unit": sig_unit,
                "Receiver": sig_recv
            })     

    # Sort lists
    messages = sorted(messages, key = lambda i: i["ID"])
    signals = sorted(signals, key = lambda i: (i["ID"], i["Startbit"], i["Name"]))
    
    # Link signals to their messages
    for m in messages:
        m["Signals"] = []
        {m.get("Signals").append(
            {x:y for x,y in s.items() if x not in ["ID"]}       # (2) Dict comprehension for single signal to remove the ID key
            ) 
            for s in signals if s.get("ID") == m.get("ID")}     # (1) Dict comprehension to filter all signals with correct ID
    
    return messages

" Convert a dbc file to xml "
def dbcToXML(filename, srcdir, dstdir):
    messages = readDBC(filename, srcdir)
    with open(dstdir+"/"+filename[:-4]+".xml","w") as file:
        
        # Root tag name -> Modify here if needed
        root = "dbc"

        # Change Indentation here. Currently 4 whitespaces = 1 tab
        indent = " " * 4

        # Write XML Header
        file.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")

        # Root opening tag
        file.write(f"<{root}>\n")

        # Write Message
        for m in messages:
            file.write(indent + "<message>\n")
            
            file.write(indent*2 + "<ID>"           + str(m.get("ID"))          + "/<ID>\n")
            file.write(indent*2 + "<fullname>"     + str(m.get("Fullname"))    + "/<fullname>\n")
            file.write(indent*2 + "<shortname>"    + str(m.get("Shortname"))  + "/<shortname>\n")
            file.write(indent*2 + "<DLC>"          + str(m.get("DLC"))         + "</DLC>\n")
            file.write(indent*2 + "<sender>"       + str(m.get("Sender"))      + "/<sender>\n")
            
            file.write(indent*2 + "<signals>\n")
            for s in m.get("Signals"):
                file.write(indent*3 + "<name>"     + str(s.get("Name"))     + "</name>\n")
                file.write(indent*3 + "<startbit>" + str(s.get("Startbit")) + "</startbit>\n")
                file.write(indent*3 + "<length>"   + str(s.get("Length"))   + "</length>\n")
                file.write(indent*3 + "<unit>"     + str(s.get("Unit"))     + "</unit>\n")
                
                for r in s.get("Receiver"):
                    file.write(indent*4 + "<receiver>" + str(r) + "</receiver>\n")

                file.write(indent*2 + "</signals>\n")

            file.write(indent + "</message>\n")

        # Root closing tag
        file.write(f"</{root}>\n")         

" Converts a dbc file to json "
def dbcToJSON(filename, srcdir, dstdir):
    messages = readDBC(filename, srcdir)
    with open(dstdir+"/"+filename[:-4]+".json","w") as file:
        json.dump(messages, file, indent=4)    

" Converts a dbc file to json, just a bit different output format "
def dbcToJSON2(filename, srcdir, dstdir):
    messages = readDBC(filename, srcdir)
    messages = sorted(messages, key = lambda i: (i["Sender"], i["ID"])) # Neu sortieren -> nach Sender, dann nach ID
    name = filename[:10] # "Norax_CanX"
    sender =  []
    for m in messages:
        sdr = m.get("Sender")
        if any(sdr in s.get("Nodename") for s in sender):
            continue;
        else:
            sender.append({
                "Nodename":sdr,
                "Messages":[]
            })
    #sender = sorted(sender, key=str.lower) # alphabetisch sortieren -> nicht mehr nötig, messages wird schon sortiert
    #sender = sorted(sender, key = lambda i: i["Nodename"])
    for s in sender:
        for m in messages:
            if (s.get("Nodename") == m.get("Sender")):
                s["Messages"].append(m)
    with open(dstdir+"/"+name+"_Nodes.json","w") as file:
        json.dump(sender, file, indent=4)             

" Converts a dbc file to c message structs "
def dbcToC(filename, srcdir, dstdir):
    messages = readDBC(filename, srcdir)
    
    with open(dstdir+"/"+"CAN_defines.h","w") as file:
        createCanDefines(file, messages)

    with open(dstdir+"/"+"MessageTypedefs.h","w") as file:
        createMessageTypedefs(file, messages)

    with open(dstdir+"/"+"CANmessage.h","w") as file:
        createCANmessage(file, messages)

    with open(dstdir+"/"+"CANdata.h", "w") as file:
        createCANdata(file, messages)
        
    # Alles alter Scheiß
    with open(dstdir+"/"+"bsp_can.h","w") as file:
        createCanH(file, messages)
    
    with open(dstdir+"/"+"bsp_can.c","w") as file:
        createCanC(file, messages)

    with open(dstdir+"/"+"bsp_gui_data.h","w") as file:
        createGuiDataH(file, messages)

    with open(dstdir+"/"+"bsp_gui_data.c","w") as file:
        createGuiDataC(file, messages)
    

" Converts a dbc file into C# Models "
def dbcToCS(filename, srcdir, dstdir):
    messages = readDBC(filename, srcdir)
    #print(file)
    can = filename.find("_Can")
    can = filename[can+1:can+5] # Start file with "CANx_" with x being the number of the bus

    with open(dstdir+"/"+ can + "_Data.cs","w") as file:
        createCS(file, can, messages)

" Creates various useful files from a dbc file for telemetry "
def dbcToPy(filename, srcdir, dstdir):
    messages = readDBC(filename, srcdir)
    with open(dstdir+"/"+"conversion_generated.py","w") as file:
        createPyConversion(file, messages)

"create bsp_can.h: message struct + typedef struct below"
def createCanH(file, messages):
    #if isinstance(file, _io.TextIOWrapper):
    #    pass
    # _io.TextIOWrapper
    # list

    # Create a struct for every message
    for m in messages:
        bits = 0 # reset when a new message is created
        
        # Write message header
        msgName = m.get("Fullname")
        ID = m.get("ID")
        file.write(f"// {msgName} {ID} \n")                           # // msg_<Message Name> 0x<ID>
        file.write(f"#define CAN_ID_{msgName} {ID} \n")               # #define CAN_ID_msg_<Message Name> 0x<ID
        file.write("typedef struct __attribute__((packed)) {" + "\n") # typedef struct __attribute__((packed)) {
        
        # Write Signals
        for s in m.get("Signals"):
            sigName = s.get("Name")
            # (1) Check if stuffing bits are required
            start = int(s.get("Startbit"))
            if start != bits:
                file.write("\t")
                diff = start - bits # difference in bits to be filled in
                if diff <= 8:
                    file.write("uint8_t ") 
                elif diff <= 16:
                    file.write("uint16_t ")
                elif diff <= 32:
                    file.write("uint32_t ")
                if diff%8 != 0 or diff > 16 and diff%32 != 0:
                    file.write(": " + str(diff))                      # uintxx_t: <bits>  (if bits are required)
                file.write(";" + "\n")                                # uintxx_t;         (if stuffing length == variable type length)
                bits += diff

            # (2) Check whether a uint8_t, uint16_t or uint32_t is required for the Signal
            file.write("\t")
            signalLength = int(s.get("Length"))
            if signalLength <= 8:
                file.write("uint8_t ") 
            elif signalLength <= 16:
                file.write("uint16_t ")
            elif signalLength <= 32:
                file.write("uint32_t ")
            file.write(sigName)

            # (3) If the signal is not exactly 8, 16 or 32 bits long -> specify bitfield length
            if signalLength%8 != 0 or signalLength > 16 and signalLength%32 != 0:
                file.write(" : " + str(signalLength))                 # uintxx_t <Signal Name>: <bits>
            file.write(";" + "\n")                                    # uintxx_t <Signal Name>; (if signal length == variable type length)

            bits+=signalLength
        # Write closing curly bracket + struct name
        file.write("}" + f" {msgName}; \n\n")                          # }  msg_<Message Name>;

    # Write the Typedef struct after messages
    file.write("typedef struct {\n")         # typedef struct {
    file.write("\tuint16_t msgType;\n")      #   uint16_t msgType;
    file.write("\tunion {\n")                #   union {
    file.write("\t\tuint8_t data[8];\n")     #       uint8_t data[8];
    for m in messages:
        file.write("\t\t" + m.get("Fullname") + " " + m.get("Fullname") + ";\n")
    file.write("\t} u;\n")              #   } messages; -> aktuell geändert zu "u"
    file.write("} bsp_generic_can_msg;\n")

"create bsp_can.c: msg IDs and Callback function"
def createCanC(file, messages):
    #print("type WriteFile:" + str(type(file)) + "\nMessages Array:" + str(type(messages)))
    # Write messages struct
    file.write("const uint16_t msg_IDs[" + str(len(messages)) + "] = {")
    for m in messages:
        file.write("\n\tCAN_ID_" + m.get("Fullname"))
        if m != messages[-1]:
            file.write(",") # write separator if this isnt the last item
    file.write("\n};\n")

    file.write("\n\n\n\n\n") # blank lines for readability

    # write Callback
    file.write("void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {\n")
    file.write("\tCAN_RxHeaderTypeDef\t\tcan_rx_header;\n\tuint8_t\t\t\t\t\tcan_rx_data[8];\n\n")
    file.write("\t// Get Message from CAN\n\tHAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &can_rx_header, can_rx_data);\n\n")
    file.write("\tbsp_generic_can_msg msg;\n\n\t// Parse Message and raise corresponding actions\n")
    file.write("\tif (bsp_can_parse_generic(&msg, &can_rx_header, can_rx_data) == HAL_OK) {\n\t\tswitch (msg.msgType) {\n\n")
    
    # switch case
    for m in messages:
        name = m.get("Fullname").split("_")
        file.write("\t\t// " + m.get("Fullname") + " " + m.get("ID") + "\n")
        file.write("\t\tcase CAN_ID_" + m.get("Fullname") + ":\n")
        
        # Sonderfälle VOR Setter hier:
        if (m.get("ID") == "0x140"):
            # AMS LED transition
            file.write("\t\t\tbsp_stmLeds_transition(&G_LED_STM,\n\t\t\t\tmsg.u.msg_VCU_StatusAMS.sig_VCU_ErrorAMS ?\n\t\t\t\t\tEVENT_SigAMSError : EVENT_SigAMSOK\n\t\t\t);\n")
            # IMD LED transition
            file.write("\t\t\tbsp_stmLeds_transition(&G_LED_STM,\n\t\t\t\tmsg.u.msg_VCU_StatusAMS.sig_VCU_ErrorIMD ?\n\t\t\t\t\tEVENT_SigIMDError : EVENT_SigIMDOK\n\t\t\t);\n")
            # msg_Dashboard_FeedbackStatus
            file.write("\t\t\tbsp_can_send_dashboard_feedback();\n")
        
        file.write("\t\t\tbsp_gui_data_set_" + str(name[2]) + "(&msg.u." + m.get("Fullname") + ");\n\t\t\tbreak;\n\n") # Aktuell "msg.messages" zu "msg.u" geändert

        # Sonderfälle NACH Setter hier:
        # ...
    
    # default case
    file.write("\t\tdefault:\n\t\t\t// Hier wenn Parse erfolgreich, aber keine ID passt\n\t\t\tbreak;\n")

    # closing curly brackets
    file.write("\t\t}\n\t}\n}\n")

"create bsp_gui_data.h: internal data structure and function declarations" 
def createGuiDataH(file, messages):
    
    # create gui data struct    
    file.write("typedef struct {\n\n")
    #print(messages)
    for m in messages:
        file.write("\t// " + m.get("Fullname") + " " + m.get("ID") + "\n\t")
        for s in m.get("Signals"):
            if s.get("Length") <= 8:
                file.write("uint8_t  ")
            elif s.get("Length") <= 16:
                file.write("uint16_t ")
            else:
                file.write("uint32_t ")
            file.write(str(s.get("Name").split("_")[2]) + ";\t// " + s.get("Name") + "\n\t")
        file.write("\n")
    file.write("} gui_data_t;\n\nextern gui_data_t* gui_data_p;\n")

    # create functions
    file.write(createHeader("Functions")) # Header "Functions"
    for m in messages:
        # function comment
        file.write("\n/*"
                   "\n  * @brief\tSets he data from CAN Message \"" + m.get("Fullname") + "\" with ID " + m.get("ID") + ""
                   "\n  *\t\t\tin gui_data-struct"
                   "\n  * @param\tmsg pointer to a message of type \"" + m.get("Fullname") + "\""
                   "\n  */\n") 
        file.write("void bsp_gui_data_set_" + m.get("Fullname").split("_")[2] + "(" + m.get("Fullname") + " *msg);\n")

"create bsp_gui_data.c: function declarations"                 
def createGuiDataC(file, messages):
    file.write(createHeader("Variables")) # Header "Variables"
    file.write("\nstatic gui_data_p gui_data_t gui_data_instance;\n")
    file.write("gui_data_t* gui_data_p = &gui_data_instance;\n")
    file.write(createHeader("Functions")) # Header "Functions"
    for m in messages:
        file.write("\n// " + m.get("Fullname") + " " + m.get("ID") + "\n")
        file.write("void bsp_gui_data_set_" + m.get("Fullname").split("_")[2] + "(" + m.get("Fullname") + " *msg) {\n")
        for s in m.get("Signals"):
            file.write("\tgui_data_p->" + s.get("Name").split("_")[2] + " = ")
            if s.get("Length") == 1:
                file.write("!!")
            file.write("msg->" + s.get("Name") + ";\n")
        file.write("}\n")

"create CAN_defines.h: can definitions (ID and DLC for each message)"
def createCanDefines(file, messages):
    # Create Header for Auto-Generated File
    file.write("\n/**\n * AUTO GENERATED - DO NOT CHANGE \n * \n * Generated using my DBC Converter: \n * https://gitlab.com/QuestGamer7/dbc_converter (Julian Rico)\n * \n * on " + str(datetime.now()) + "\n * \n */\n") # Autogenerated Hinweis
    # Info for user where to put code
    file.write("\n// Before pasting this file, check the existing file for user additions.")
    file.write("\n// Please paste this file in the src/inc folder of your project.\n")
    # pragma once
    file.write("\n#pragma once\n") # "#pragma once"
    # Header for CAN Definitions
    file.write(createHeader("CAN Definitions")) # Header "Definitions"
    file.write("\n") # Leerzeile

    # TODO: Schön formattieren -> Mit Tabulatoren arbeiten und die korrekte Stelle des Tabs berechnen    
    nr = 1;
    for m in messages:
        string = "CAN" + str(nr) + "_" + (m.get("Fullname"))[4:] + "_MSG"
        file.write("#define " + string + "_ID" + " "*(48-len(string)-3) + m.get("ID") + "\n")           # Nach String 48-(Stringlänge von "CAN_..." + "_ID") Leerzeichen einfügen
        file.write("#define " + string + "_DL" + " "*(48-len(string)-3) + str(m.get("DLC")) + "\n")     # Nach String 48-(Stringlänge von "CAN_..." + "_DL") Leerzeichen einfügen
        file.write("\n")

"create MessageTypedefs: Typedef definitions (bitfields) for each message"
def createMessageTypedefs(file, messages):
    # Create Header for Auto-Generated File
    file.write("\n/**\n * AUTO GENERATED - DO NOT CHANGE \n * \n * Generated using my DBC Converter: \n * https://gitlab.com/QuestGamer7/dbc_converter (Julian Rico)\n * \n * on " + str(datetime.now()) + "\n * \n */\n") # Autogenerated Hinweis
    # Info for user where to put code
    file.write("\n// Before pasting this file, check the existing file for user additions.")
    file.write("\n// Please paste this file in the src/inc folder of your project.\n")
    # pragma once
    file.write("\n#pragma once\n") # "#pragma once"
    # Header for Typedefs
    file.write(createHeader("Typedefs")) # Header "Typedefs"
    file.write("\n") # Leerzeile
  
    # Create a struct for every message
    for m in messages:
        bits = 0 # reset when a new message is created
        
        # Write message header
        msgName = m.get("Fullname")
        ID = m.get("ID")
        file.write(f"// {msgName} {ID} \n")                           # // msg_<Message Name> 0x<ID>
        # file.write(f"#define CAN_ID_{msgName} {ID} \n")               # #define CAN_ID_msg_<Message Name> 0x<ID
        file.write("typedef struct __attribute__((packed)) {" + "\n") # typedef struct __attribute__((packed)) {
        
        # Write Signals
        for s in m.get("Signals"):
            sigName = s.get("Name")
            # (1) Check if stuffing bits are required
            start = int(s.get("Startbit"))
            if start != bits:
                file.write("\t")
                diff = start - bits # difference in bits to be filled in
                if diff <= 8:
                    file.write("uint8_t ") 
                elif diff <= 16:
                    file.write("uint16_t ")
                elif diff <= 32:
                    file.write("uint32_t ")
                if diff%8 != 0 or diff > 16 and diff%32 != 0:
                    file.write(": " + str(diff))                      # uintxx_t: <bits>  (if bits are required)
                file.write(";" + "\n")                                # uintxx_t;         (if stuffing length == variable type length)
                bits += diff

            # (2) Check whether a uint8_t, uint16_t or uint32_t is required for the Signal
            file.write("\t")
            signalLength = int(s.get("Length"))
            if signalLength <= 8:
                file.write("uint8_t ") 
            elif signalLength <= 16:
                file.write("uint16_t ")
            elif signalLength <= 32:
                file.write("uint32_t ")
            file.write(sigName)

            # (3) If the signal is not exactly 8, 16 or 32 bits long -> specify bitfield length
            if signalLength%8 != 0 or signalLength > 16 and signalLength%32 != 0:
                file.write(" : " + str(signalLength))                 # uintxx_t <Signal Name>: <bits>
            file.write(";" + "\n")                                    # uintxx_t <Signal Name>; (if signal length == variable type length)

            bits+=signalLength
        # Write closing curly bracket + struct name
        file.write("}" + f" {msgName}; \n\n")                          # }  msg_<Message Name>;

"create can_msg_t: Definiton for a CAN message with all message types"
def createCANmessage(file, messages):
    # Create Header for Auto-Generated File
    file.write("\n/**\n * AUTO GENERATED - DO NOT CHANGE \n * \n * Generated using my DBC Converter: \n * https://gitlab.com/QuestGamer7/dbc_converter (Julian Rico)\n * \n * on " + str(datetime.now()) + "\n * \n */\n") # Autogenerated Hinweis
    # Info for user where to put code
    file.write("\n// This part of code goes in can_if.h as last part of the typedefs.\n")
    # pragma once
    file.write("\n#pragma once\n") # "#pragma once"
    # Header for Typedefs
    file.write(createHeader("Typedefs")) # Header "Typedefs"
    file.write("\n") # Leerzeile

    # Write the Typedef struct after messages
    file.write("typedef struct {\n")         # typedef struct {
    file.write("\tuint16_t msgType;\n")      #   uint16_t msgType;
    file.write("\tunion {\n")                #   union {
    file.write("\t\tuint8_t data[8];\n")     #       uint8_t data[8];
    for m in messages:
        file.write("\t\t" + m.get("Fullname") + " " + m.get("Fullname") + ";\n")
    file.write("\t} u;\n")              #   } messages; -> aktuell geändert zu "u"
    file.write("} can_msg_t;\n")

"create can_data_t: Definiton for all the CAN data"
def createCANdata(file, messages):
    # Create Header for Auto-Generated File
    file.write("\n/**\n * AUTO GENERATED - DO NOT CHANGE \n * \n * Generated using my DBC Converter: \n * https://gitlab.com/QuestGamer7/dbc_converter (Julian Rico)\n * \n * on " + str(datetime.now()) + "\n * \n */\n") # Autogenerated Hinweis
    # Info for user where to put code
    file.write("\n// This part of code goes in app_funcitons.h.\n")
    # pragma once
    file.write("\n#pragma once\n") # "#pragma once"
    # Header for Typedefs
    file.write(createHeader("Typedefs")) # Header "Typedefs"
    file.write("\n") # Leerzeile

 # create gui data struct    
    file.write("/* Data from CAN busses */\n")
    file.write("typedef struct {\n\n")
    #print(messages)
    for m in messages:
        file.write("\t// " + m.get("Fullname") + " " + m.get("ID") + "\n\t")
        for s in m.get("Signals"):
            if s.get("Length") <= 8:
                file.write("uint8_t  ")
            elif s.get("Length") <= 16:
                file.write("uint16_t ")
            else:
                file.write("uint32_t ")
            file.write(str(s.get("Name").split("_")[2]) + ";\t// " + s.get("Name") + "\n\t")
        file.write("\n")
    file.write("} can_data_t;\n\nextern can_data_t* can_data_p;\n")

"create Data.cs: CAN Data for Models"
def createCS(file, name, messages): 
    # create gui data struct    
    file.write("public struct " + name.upper() + "\n{\n")
    #print(messages)
    for m in messages:
        file.write("\t// " + m.get("Name") + " " + m.get("ID") + "\n\t")
        for s in m.get("Signals"):
            if s.get("Length") == 8:
                file.write("bool ")
            else:
                file.write("double ")
            file.write(str(s.get("Name").split("_")[2]) + " { get; set; }" + "\t// " + s.get("Name") + "\n\t")
        file.write("\n")
    file.write("}\n")

"create conversion.py: Converts incoming bus signals into values for database"
def createPyConversion(file, messages):
    # TODO: Debugausgabe in conversion.py => Debugbit übergeben
    file.write("# AUTO-GENERATED by DBC Converter - DO NOT CHANGE\n")
    file.write("# This file creates an sql string for storing a can message in the telemetry database\n")
    file.write("def switch(id, data):\n")
    file.write("\tif id==0:\n\t\treturn conv_0(data)\n")
    
    for m in messages: 
        id = str(int(m.get("ID"), 16))
        file.write("\telif id==" + id + ": # " + m.get("ID") + "\n\t\treturn conv_" + id + "(data)\n")
    
    for m in messages:

        # DEBUG -> nur 1 Message (leichter für prints etc)
        #if (int(m.get("ID"),16) != 305):
        #    continue
        # DEBUG ENDE

        file.write("\n\" Data conversion for message " + m.get("ID") + ": " + m.get("Fullname") + " \"\n")
        id = int(m.get("ID"), 16)
        file.write("def conv_{:d}(data):\n\tdlc = {:d}\n\tsum_signals = {:d}\n\n".format(id, m.get("DLC"), len(m.get("Signals"))))
        
        file.write("\tsql = \"CALL STORE3({:d}\" # Aktuell noch STORE3 -> auf normale STORE aendern!\n\n".format(id))
        
        valueslot = 0
        for s in m.get("Signals"):
            file.write("\t# value_slot[" + str(valueslot) + "]: " + s.get("Name") + "\n")
            file.write("\tsql += \",\" + str(" + str(s.get("Factor")) + "*(")
            
            start = s.get("Startbit")   # Startbit
            length = s.get("Length")    # Signallänge in Bits
            shiftR = start%8            # Verschiebung des ersten Byte um x Bits nach rechts
            byte = int(start/8)         # Aktuelles Datenbyte

            # 1. Byte
            file.write("data[{:d}]".format(byte))

            # Erstes Byte verschieben, wenn Startbit nicht LSB eines vollen Bytes
            #if (shiftR): -> Wird jetzt bei jedem 1. Byte dazu geschrieben, schaut insgesamt besser aus
            file.write(" >> {:d}".format(shiftR))
                
            # Bitmaskierung des ersten Bytes
            mask = min(length, 8 - shiftR) # Maskierung: Entweder Restbits von Shift oder Länge (z.B. bei 1 Bit breiten Signalen)
            if (mask == 0): # mask = 0 OR mask = 8
                print("FEHLER: Signallänge 0")

            elif (mask < 8 and mask == length): # 0 < mask < 8; Nur Bits maskieren, die nicht zu einem nächsten Byte gehören
                file.write(" & 0x{:0>2X}".format(int((2**mask)-1)))
            
            # Prüfen, ob noch ein Byte angehängt werden muss
            restbits = length - mask
            if (restbits): 
                restbytes = 1 + int((restbits-1)/8)
                for i in range(restbytes):
                    byte += 1
                    file.write(" + data[{:d}]".format(byte))
                    # Prüfen ob es das letzte Byte ist und evtl. maskiert werden muss
                    if (restbits < 8):
                        file.write(" & 0x{:0>2X}".format((2**restbits)-1))
                    file.write(" << {:d}".format(8*(i+1)))
                    restbits -= 8

            file.write("))\n") # Klammer von Multiplikation und str() schließen

            valueslot += 1      # value_slot[..] inkrementieren
      
        file.write("\n\t# Restliche value_slots mit 0 belegen -> muss drin bleiben bis default werte in SP hinterlegt sind\n")
        file.write("\tfor i in range(10-sum_signals):\n\t\tsql += \",\" + \"0\"\n\n")
        file.write("\tsql += \");\"\n")
        file.write("\t#print(sql) # TODO: Debugausgabe \n\n")
        file.write("\treturn sql\n")    
    
"logging, needs work"
# type: 0 = string, 1 = messages
def log(dstdir, toLog, type):
    with open(dstdir+"/log.log","w") as file:
        if type==0:
            file.write(toLog);
        elif type == 1:
            for m in toLog:
                file.write(m)    