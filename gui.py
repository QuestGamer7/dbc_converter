"""
This Module provides a GUI for quick and easy conversion of a dbc file into other formats.
PyQt5 is used for creating the GUI.
"""

# imports
import sys
import os.path
import dbc_converter as converter
from pathlib import Path
from PyQt5.QtWidgets    import QApplication, QWidget, QPushButton, QLabel, QLineEdit, QSizePolicy
from PyQt5.QtWidgets    import QGridLayout, QFileDialog, QCheckBox, QMessageBox, QStyle
from PyQt5.QtCore       import Qt, QUrl, QRect
from PyQt5.QtGui        import QFont, QIcon, QPixmap

class MyWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()      

    def initUI(self):
        self.messages = []
        self.srcdir = os.getcwd()
        self.dstdir = self.srcdir
        self.CANfile = "CanX_ChnX_..."
        self.CANnumber = 0
        self.file="..."
        self.startDirectory=""  
        self.setWindowTitle("DBC Converter")
        self.setWindowFlag(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        grid = QGridLayout()

        # if only 1 dbc file exists -> set file and output path accordingly
        dbc = [file for file in os.listdir(self.srcdir) if file[-4:] == ".dbc"]
        if len(dbc) == 1:
            self.file = dbc[0]
        
        # initialize widgets
        imgMain = QLabel(self)
        pixmap = QPixmap("img/logo.png")
        imgMain.setPixmap(pixmap)
        imgMain.setFixedSize(129,129)        
        imgMain.setScaledContents(True)
        lblTitle = QLabel("DBC Converter", self)
        lblTitle.setFont(QFont("Arial", 20, 1000,False))
        lblTitle.setStyleSheet("color:blue")
        lblSteps = QLabel(  "Anleitung:\n"
                            "1. DBC Datei auswählen\n"
                            "2. Datei einlesen\n"
                            "2. Ausgabeformat wählen\n"
                            "3. Konvertieren!")
        lblSteps.setFont(QFont("Arial", 10, italic=False))
        self.btnOpenFile = QPushButton("Choose File", self)
        self.btnChangeDir = QPushButton("Change Output Directory", self)
        self.lblFile = QLabel(self.file, self)
        self.lblDirectory = QLabel(self.dstdir, self)        
        self.btnReadFile = QPushButton("Einlesen", self)
        self.lblReadFile = QLabel("Noch nicht eingelesen...", self)
        self.cbXML = QCheckBox("XML", self)
        self.cbJSON = QCheckBox("JSON", self)
        self.cbCStruct = QCheckBox("C Code", self)
        self.cbCS = QCheckBox("C#", self)
        self.cbPY = QCheckBox("Python", self)
        self.btnConvert = QPushButton("Convert!", self)
        
        # initialize grid
        grid.addWidget(imgMain,             0, 0, 2, 1, alignment=Qt.AlignCenter)
        grid.addWidget(lblTitle,            0, 1, 1, 4, alignment=Qt.AlignLeft)
        grid.addWidget(lblSteps,            1, 1, 1, 4)
        grid.addWidget(self.btnOpenFile,    2, 0)   
        grid.addWidget(self.lblFile,        2, 1, 1, 4)
        grid.addWidget(self.btnReadFile,    3, 0)
        grid.addWidget(self.lblReadFile,    3, 1, 1, 4)
        grid.addWidget(self.btnChangeDir,   4, 0)     
        grid.addWidget(self.lblDirectory,   4, 1, 1, 4)
        grid.addWidget(self.btnConvert,     5, 0, 2, 1)
        grid.addWidget(self.cbXML,          5, 1)
        grid.addWidget(self.cbJSON,         5, 2)
        grid.addWidget(self.cbCStruct,      5, 3)
        grid.addWidget(self.cbCS,           5, 4)
        grid.addWidget(self.cbPY,           6, 1)
        
        # eventhandlers
        self.btnOpenFile.clicked.connect(self.openFileDialog)
        self.btnReadFile.clicked.connect(self.readFile)
        self.btnChangeDir.clicked.connect(self.openDirDialog)
        self.btnConvert.clicked.connect(self.convert)
            
        self.setLayout(grid)
        self.show()

    def openFileDialog(self):
        file = QFileDialog(self, "Choose DBC File...").getOpenFileNames(filter="DBC Files (*.dbc)")
        if file[0]:
            file = file[0][0]
            idx = file.rfind("/")
            self.file = file[idx+1:]
            self.CANfile = Path(self.file).stem
            self.CANnumber = 1; # ToDo retrieve number of CAN Bus
            self.srcdir = file[:idx]
            self.dstdir = self.srcdir + '/' + self.CANfile
            self.lblFile.setText(self.file)
            self.lblDirectory.setText(self.dstdir)
            # Create destination repository for generated files, named after the selected CAN file
            Path(self.dstdir).mkdir(parents=True, exist_ok=True)
            #print(self.dstdir)
            #print(self.file)
            #print(self.CANfile)
            
        
    def readFile(self):
        # try/except block
        # TODO: Checks einbauen dass Pfad und Datei korrekt sind
        self.messages = converter.readDBC(self.file, self.srcdir)
        self.lblReadFile.setText("Erfolgreich eingelesen!")
        msg = QMessageBox()
        msg.setText("Einlesen erfolgreich!")
        msg.exec()
    
    def openDirDialog(self):
        path = QFileDialog(self, "Change Output Directory...").getExistingDirectory()
        print(path)
        if path:
            self.dstdir= path
            self.lblDirectory.setText(self.dstdir)
    
    def convert(self):
        msg = QMessageBox()
        if (not self.cbXML.isChecked()
            and not self.cbJSON.isChecked() 
            and not self.cbCStruct.isChecked() 
            and not self.cbCS.isChecked() 
            and not self.cbPY.isChecked()):
            msg.setWindowTitle("No output format selected")
            msg.setText("You have to choose at least one output format!")
            #msg.setDetailedText("Please click on one of the Checkboxes to choose the Output Format(s).")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.setIcon(QMessageBox.Warning)
            if msg.exec():
                return
        
        if self.file != "..." and self.srcdir:
            # Create destination directory if not exists yet
            Path(self.dstdir).mkdir(parents=True, exist_ok=True)

            # Convert to XML
            if self.cbXML.isChecked():
                converter.dbcToXML(self.file, self.srcdir, self.dstdir)
                msg.setText("XML created!")
                msg.exec()
            # Convert to JSON
            if self.cbJSON.isChecked():
                converter.dbcToJSON(self.file, self.srcdir, self.dstdir)
                converter.dbcToJSON2(self.file, self.srcdir, self.dstdir)
                msg.setText("JSON created!")
                msg.exec()
            # Convert to C Message Structs
            if self.cbCStruct.isChecked():
                converter.dbcToC(self.file, self.srcdir, self.dstdir)
                msg.setText("C Code created!")
                msg.exec()
            # Convert to C# Data Struct
            if self.cbCS.isChecked():
                converter.dbcToCS(self.file, self.srcdir, self.dstdir)
                msg.setText("C# Code created!")
                msg.exec()
            # Convert to Python files
            if self.cbPY.isChecked():
                converter.dbcToPy(self.file, self.srcdir, self.dstdir)
                msg.setText("Python files created!")
                msg.exec()

            # Inform about successful conversion
            msg.setWindowTitle("Success")
            msg.setStandardButtons(QMessageBox.Ok)

            
        else:
            msg.setWindowTitle("File or Directory invalid")
            msg.setText("No File or Output Path selected!")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.setIcon(QMessageBox.Warning)
            if msg.exec():
                return
        
app = QApplication(sys.argv)
w=MyWidget()
sys.exit(app.exec_())